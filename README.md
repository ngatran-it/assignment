# Stock Price Service

To run this service, please follow these commands :

1. Prepare environment:
Make sure Java is installed properly on the system.
Java version is 1.8 or above.
Command to check Java version: `java -version`

2. Run Stock Price Service:
Command to run: `java -jar {path to stockPriceService.jar}`
Example: java -jar C:\stockPriceService.jar
The service will start on port 8080.

3. APIs list:
- Get close prices list for specific ticker symbol:
{server_address:server_port}/api/v2/{ticker_symbol}/closePrice?startDate={start_date}&endDate={end_date}
Example: http://localhost:8080/api/v2/GE/closePrice?startDate=2018-01-01&endDate=2018-01-14
Validation:
  start_date and end_date should follow YYYY-MM-DD format.
  start_date should not be after end_date.
  
- Get 200 Day Moving Average for specific ticker symbol:
{server_address:server_port}/api/v2/{ticker_symbol}/200dma?startDate={start_date}
Example: http://localhost:8080/api/v2/GE/200dma?startDate=2018-01-14
Validation:
  start_date should follow YYYY-MM-DD format.
  
- Get 200 Day Moving Average for a list of ticker symbols.
{server_address:server_port}/api/v2/200dma?startDate={start_date}&tickerSymbolList={ticker_symbol_list}
Example: http://localhost:8080/api/v2/200dma?startDate=2018-01-14&tickerSymbolList=AAPL,GE
Validation:
  start_date should follow YYYY-MM-DD format.
  ticker_symbol_list should have this format: ticker_symbol_1, ticker_symbol_2,...ticker_symbol_n
  total element of ticker_symbol_list should not be greater than 1000.
