package com.sentifi.assignment.stockPriceService;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Main class for Stock Price Service.
/***************************************************************************/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockPriceServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockPriceServiceApplication.class, args);
	}
}

