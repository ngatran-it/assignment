package com.sentifi.assignment.stockPriceService.config;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to config cache management.
/***************************************************************************/
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.sentifi.assignment.stockPriceService.constants.ApplicationConstants;

@Configuration
@EnableCaching
public class CacheManagerConfiguration {
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        // Maximum size of cache is 10000
        // Expiration time is 1 day
        CaffeineCache cache = new CaffeineCache("closePrices", Caffeine.newBuilder()
                .maximumSize(ApplicationConstants.CACHE_SIZE)
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build());
        
        cacheManager.setCaches(Arrays.asList(cache));
        return cacheManager;
    }
}
