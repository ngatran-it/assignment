package com.sentifi.assignment.stockPriceService.constants;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to declare common application constants.
/***************************************************************************/

public abstract class ApplicationConstants {
    
    public static final String QUANDL_DATASET_WIKI_URL = "https://www.quandl.com/api/v3/datasets/WIKI";
    public static final String QUANDL_API_KEY          = "CqGx8dKZqmLsHkHzPsZV";

    public static final String YYYYMMDD_DATE_FORMAT    = "yyyy-MM-dd";
    public static final String COMMA                   = ",";
    public static final long   CACHE_SIZE              = 10_000L;
    
}
