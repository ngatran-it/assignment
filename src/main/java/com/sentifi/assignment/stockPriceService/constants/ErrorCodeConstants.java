package com.sentifi.assignment.stockPriceService.constants;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to declare error codes and strings.
/***************************************************************************/

public class ErrorCodeConstants {
    
    public static final String INVALID_STARTDATE_FORMAT_CODE        = "S10010";
    public static final String INVALID_STARTDATE_FORMAT_STRING      = "Invalid Start Date. Should follow YYYY-MM-DD format";

    public static final String INVALID_ENDDATE_FORMAT_CODE          = "S10020";
    public static final String INVALID_ENDDATE_FORMAT_STRING        = "Invalid End Date. Should follow YYYY-MM-DD format";

    public static final String INVALID_DATE_RANGE_CODE              = "S10030";
    public static final String INVALID_DATE_RANGE_STRING            = "Invalid Date Range. Start Date should not be after End Date";

    public static final String INVALID_STARTDATE_CLOSE_PRICE_CODE   = "S10040";
    public static final String INVALID_STARTDATE_CLOSE_PRICE_STRING = "No valid data for the Start Date. Please use other day such as %s";

    public static final String INVALID_TICKER_SYMBOL_LIST_CODE      = "S10050";
    public static final String INVALID_TICKER_SYMBOL_LIST_STRING    = "Ticker Symbols List only supports up to 1000. Please remove at least %s symbol(s)";

}
