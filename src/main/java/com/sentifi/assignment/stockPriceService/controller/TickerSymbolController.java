package com.sentifi.assignment.stockPriceService.controller;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to handle ticker symbol requests.
/***************************************************************************/

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sentifi.assignment.stockPriceService.service.TickerSymbolService;
import com.sentifi.assignment.stockPriceService.validators.ErrorResponse;
import com.sentifi.assignment.stockPriceService.validators.ValidateDateRange;
import com.sentifi.assignment.stockPriceService.validators.ValidateEndDate;
import com.sentifi.assignment.stockPriceService.validators.ValidateStartDate;
import com.sentifi.assignment.stockPriceService.validators.ValidateTickerSymbolList;

@RestController
@RequestMapping(value = "/api/v2")
public class TickerSymbolController {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    TickerSymbolService tickerSymbolService;
    
    /**
     * Handle request close prices for specific ticker symbol.
     * 
     * @param tickerSymbol
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping("/{tickerSymbol}/closePrice")
    public ResponseEntity<Object> getStockClosePriceByDateRange(@PathVariable String tickerSymbol,
            @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {
        logger.debug("ENTERING <getStockClosePriceByDateRange>");
        ResponseEntity<Object> responseEntity = null;
        
        // Validate startDate and endDate.
        List<ErrorResponse> errorList = new ArrayList<>();
        ErrorResponse startDateValidator = ValidateStartDate.checkValid(startDate);
        ErrorResponse endDateValidator = ValidateEndDate.checkValid(endDate);
        
        if (startDateValidator.getCode() != null) {
            errorList.add(startDateValidator);
        }
        if (endDateValidator.getCode() != null) {
            errorList.add(endDateValidator);
        }
        // Only check date range if startDate and endDate are valid.
        if (errorList.isEmpty()) {
            ErrorResponse dateRangeValidator = ValidateDateRange.checkValid(startDate, endDate);
            if (dateRangeValidator.getCode() != null) {
                errorList.add(dateRangeValidator);
            }
        }
        
        if (errorList.isEmpty()) {
            // Call service to process API request.
            responseEntity = tickerSymbolService.getStockClosePriceByDateRange(tickerSymbol, startDate, endDate);
        } else {
            // Return error list.
            responseEntity = new ResponseEntity<Object>(errorList, HttpStatus.NOT_FOUND);
        }
        
        logger.debug("EXITING <getStockClosePriceByDateRange>");
        return responseEntity;
    }
    
    /**
     * Handle 200 Day Moving Average for specific ticker symbols.
     * 
     * @param tickerSymbol
     * @param startDate
     * @return
     */
    @GetMapping("/{tickerSymbol}/200dma")
    public ResponseEntity<Object> getStock200dmaByStartDate(@PathVariable String tickerSymbol,
            @RequestParam("startDate") String startDate) {
        logger.debug("ENTERING <getStock200dmaByStartDate>");
        ResponseEntity<Object> responseEntity = null;
        
        // Validate startDate
        List<ErrorResponse> errorList = new ArrayList<>();
        ErrorResponse startDateValidator = ValidateStartDate.checkValid(startDate);
        
        if (startDateValidator.getCode() != null) {
            errorList.add(startDateValidator);
        }
        
        if (errorList.isEmpty()) {
            // Call service to process API request.
            responseEntity = tickerSymbolService.getStock200dmaByStartDate(tickerSymbol, startDate);
        } else {
            // Return error list.
            responseEntity = new ResponseEntity<Object>(errorList, HttpStatus.NOT_FOUND);
        }
        
        logger.debug("EXITING <getStock200dmaByStartDate>");
        return responseEntity;
    }
    
    /**
     * Handle 200 Day Moving Average for a list of ticker symbols up to 1000.
     * 
     * @param startDate
     * @param tickerSymbolList
     * @return
     */
    @GetMapping("/200dma")
    public Object get200dmaByStartDateForMultiTickerSymbols(@RequestParam("startDate") String startDate,
            @RequestParam("tickerSymbolList") String tickerSymbolList) {
        logger.debug("ENTERING <get200dmaByStartDateForMultiTickerSymbols>");
        ResponseEntity<Object> responseEntity = null;
        
        // Validate startDate
        List<ErrorResponse> errorList = new ArrayList<>();
        ErrorResponse startDateValidator = ValidateStartDate.checkValid(startDate);
        
        // Validate tickerSymbolList
        ErrorResponse tickerSymbolListValidator = ValidateTickerSymbolList.checkValid(tickerSymbolList);
        
        if (startDateValidator.getCode() != null) {
            errorList.add(startDateValidator);
        }
        
        if (tickerSymbolListValidator.getCode() != null) {
            errorList.add(tickerSymbolListValidator);
        }
        
        if (errorList.isEmpty()) {
            // Call service to process API request.
            responseEntity = tickerSymbolService.get200dmaByStartDateForMultiTickerSymbols(startDate, tickerSymbolList);
        } else {
            // Return error list.
            responseEntity = new ResponseEntity<Object>(errorList, HttpStatus.NOT_FOUND);
        }
        
        logger.debug("EXITING <get200dmaByStartDateForMultiTickerSymbols>");
        return responseEntity;
    }
}
