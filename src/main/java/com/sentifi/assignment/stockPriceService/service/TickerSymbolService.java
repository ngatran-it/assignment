package com.sentifi.assignment.stockPriceService.service;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to execute ticker symbol requests.
/***************************************************************************/

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.sentifi.assignment.stockPriceService.constants.ApplicationConstants;
import com.sentifi.assignment.stockPriceService.constants.ErrorCodeConstants;
import com.sentifi.assignment.stockPriceService.util.TimeStringUtil;
import com.sentifi.assignment.stockPriceService.validators.ErrorResponse;
import com.sentifi.assignment.stockPriceService.values.PriceDisplayVO;
import com.sentifi.assignment.stockPriceService.values.QuandlResponseDisplayVO;
import com.sentifi.assignment.stockPriceService.values.TickerSymbolDisplayVO;

@Service
@Cacheable(value = "closePrices")
public class TickerSymbolService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    /**
     * Execute Quandl request to get close prices for specific ticker symbol.
     * 
     * @param tickerSymbol
     * @param startDate
     * @param endDate
     * @return
     */
    public ResponseEntity<Object> getStockClosePriceByDateRange(String tickerSymbol, String startDate, String endDate) {
        logger.debug("ENTERING <getStockClosePriceByDateRange>");
        
        // Call quandl API to get Close Price for specific Ticker Symbol
        String columnIndex = "4"; // For Close Price
        String url = String.format("%s/%s.json?api_key=%s&column_index=%s&start_date=%s&end_date=%s",
                ApplicationConstants.QUANDL_DATASET_WIKI_URL, tickerSymbol, ApplicationConstants.QUANDL_API_KEY,
                columnIndex, startDate, endDate);

        ResponseEntity<Object> responseEntity = null;
        Gson gson = new Gson();
        try {
            responseEntity = new RestTemplate().getForEntity(url, Object.class);;
            
            // Convert quandl response to json
            String json = gson.toJson(responseEntity.getBody());
            
            // Convert json to value object
            QuandlResponseDisplayVO quandlResponseDisplayVO = gson.fromJson(json, QuandlResponseDisplayVO.class);
            
            // Build constructor for displaying values.
            TickerSymbolDisplayVO tickerSymbolDisplayVO = new TickerSymbolDisplayVO();
            List<PriceDisplayVO> priceList = new ArrayList<>();
            tickerSymbolDisplayVO.setPrices(priceList);
            
            PriceDisplayVO priceDisplayVO = new PriceDisplayVO();
            priceList.add(priceDisplayVO);
    
            // Set values from quandl response.
            tickerSymbolDisplayVO.setNewestAvailableDate(quandlResponseDisplayVO.getDataset().getNewest_available_date());
            
            priceDisplayVO.setTicker(tickerSymbol);
            priceDisplayVO.setDateClose(quandlResponseDisplayVO.getDataset().getData());
            
            responseEntity = new ResponseEntity<Object>(tickerSymbolDisplayVO, HttpStatus.OK);
        } catch (HttpClientErrorException ex) {
            // Return quandl error to client.
            QuandlResponseDisplayVO quandlResponseDisplayVO = gson.fromJson(ex.getResponseBodyAsString(), QuandlResponseDisplayVO.class);
            responseEntity = new ResponseEntity<Object>(quandlResponseDisplayVO.getQuandl_error(), HttpStatus.NOT_FOUND);
        }
        
        logger.debug("EXITING <getStockClosePriceByDateRange>");
        
        return responseEntity;
    }

    /**
     * Execute Quandl request to get 200 Day Moving Average for specific ticker symbol.
     * 
     * @param tickerSymbol
     * @param startDateStr
     * @return
     */
    public ResponseEntity<Object> getStock200dmaByStartDate(String tickerSymbol, String startDateStr) {
        logger.debug("ENTERING <getStock200dmaByStartDate>");
        
        ResponseEntity<Object> response;
        
        // Convert startDate to Date format.
        Date startDate = null;
        try {
            startDate = TimeStringUtil.parseStringToDate(startDateStr);
        } catch (Exception e) {
            // Ignore since we already check valid startDate in controller;
        }
        
        Calendar cal = Calendar.getInstance();
        Date fromDate;

        // Get close price in 40 weeks duration.
        cal.setTime(startDate);
        cal.add(Calendar.DAY_OF_MONTH, -279);
        fromDate = cal.getTime();
        String fromDateStr = TimeStringUtil.parseDateToString(fromDate);
        
        ResponseEntity<Object> closePriceResponse = getStockClosePriceByDateRange(tickerSymbol, fromDateStr, startDateStr);
        
        if (closePriceResponse.getStatusCode() != HttpStatus.OK) {
            // Redirect error list.
            response = closePriceResponse;
        } else {
            // Get valid result.
            TickerSymbolDisplayVO tickerSymbolDisplayVO = (TickerSymbolDisplayVO) closePriceResponse.getBody();
            List<PriceDisplayVO> priceList = tickerSymbolDisplayVO.getPrices();
            PriceDisplayVO priceDisplayVO = priceList.get(0);
            List<List<String>> dateCloseList = priceDisplayVO.getDateClose();
            
            // Check if there is no data for the start date.
            if (!dateCloseList.isEmpty()) {
                // Calculate average value.
                double averageValue = dateCloseList.stream()
                        .mapToDouble(dateClose -> Double.parseDouble(dateClose.get(1)))
                        .summaryStatistics()
                        .getAverage();
                
                // Prepare response to display
                tickerSymbolDisplayVO = new TickerSymbolDisplayVO();
                priceDisplayVO = new PriceDisplayVO();
                priceDisplayVO.setTicker(tickerSymbol);
                priceDisplayVO.setAvg(String.format("%.2f", averageValue));
                tickerSymbolDisplayVO.setDma200(priceDisplayVO);
                
                response = new ResponseEntity<Object>(tickerSymbolDisplayVO, HttpStatus.OK);
            } else {
                // Throw error and suggest the first possible start date.
                List<ErrorResponse> errorList = new ArrayList<>();
                ErrorResponse error = new ErrorResponse();
                errorList.add(error);
                
                String firstAvailableDateStr = tickerSymbolDisplayVO.getNewestAvailableDate();

                error.setCode(ErrorCodeConstants.INVALID_STARTDATE_CLOSE_PRICE_CODE);
                error.setMessage(String.format(ErrorCodeConstants.INVALID_STARTDATE_CLOSE_PRICE_STRING, firstAvailableDateStr));
                
                response = new ResponseEntity<Object>(errorList, HttpStatus.NOT_FOUND);
            }
        }
        
        logger.debug("EXITING <getStock200dmaByStartDate>");
        
        return response;
    }

    /**
     * Execute Quandl request to get 200 Day Moving Average for a list of ticker symbols.
     * 
     * @param startDate
     * @param tickerSymbolList
     * @return
     */
    public ResponseEntity<Object> get200dmaByStartDateForMultiTickerSymbols(String startDate, String tickerSymbolList) {
        logger.debug("ENTERING <get200dmaByStartDateForMultiTickerSymbols>");
        
        // Get ticker symbols list
        String[] tickerSymbols = tickerSymbolList.split(ApplicationConstants.COMMA);
        List<PriceDisplayVO> priceList = new ArrayList<>();
        
        // Execute 200dma per ticker symbol
        ResponseEntity<Object> closePriceResponse;
        TickerSymbolDisplayVO tickerSymbolDisplayVO;
        PriceDisplayVO priceDisplayVO;
        List<Object> errorList;
        for (String tickerSymbol : tickerSymbols) {
            if (tickerSymbol.trim().isEmpty()) {
                // Do not process for empty ticker symbol.
                continue;
            }
            closePriceResponse = getStock200dmaByStartDate(tickerSymbol.trim(), startDate);
            if (closePriceResponse.getStatusCode() == HttpStatus.OK) {
                tickerSymbolDisplayVO = (TickerSymbolDisplayVO) closePriceResponse.getBody();
                priceDisplayVO = tickerSymbolDisplayVO.getDma200();
            } else {
                // Generate error message
                errorList = new ArrayList<>();
                priceDisplayVO = new PriceDisplayVO();
                priceDisplayVO.setTicker(tickerSymbol);
                errorList.add(closePriceResponse.getBody());
                
                priceDisplayVO.setErrorList(errorList);
            }
            priceList.add(priceDisplayVO);
        }
        
        logger.debug("EXITING <get200dmaByStartDateForMultiTickerSymbols>");
        
        return new ResponseEntity<Object>(priceList, HttpStatus.OK);
    }

}
