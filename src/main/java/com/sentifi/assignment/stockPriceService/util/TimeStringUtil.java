package com.sentifi.assignment.stockPriceService.util;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to convert between formatted string and date.
/***************************************************************************/

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sentifi.assignment.stockPriceService.constants.ApplicationConstants;

public abstract class TimeStringUtil {

    private static final SimpleDateFormat SDF            = new SimpleDateFormat(ApplicationConstants.YYYYMMDD_DATE_FORMAT);
    
    public static Date parseStringToDate(String dateString) throws ParseException {
        // Strictly parsing
        SDF.setLenient(false);
        return SDF.parse(dateString);
    }
    
    public static String parseDateToString(Date date) {
        return SDF.format(date);
    }
}
