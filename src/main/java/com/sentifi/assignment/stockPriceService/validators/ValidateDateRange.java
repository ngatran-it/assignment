package com.sentifi.assignment.stockPriceService.validators;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to validate date range.
 * 
 *  RULES:          1) Make sure startDate and endDate are valid before.
 *                  2) startDate should not be after endDate.
 */
/***************************************************************************/

import java.util.Date;

import com.sentifi.assignment.stockPriceService.constants.ErrorCodeConstants;
import com.sentifi.assignment.stockPriceService.util.TimeStringUtil;

public abstract class ValidateDateRange {

    public static ErrorResponse checkValid(String startDateStr, String endDateStr) {
        ErrorResponse errorResponse = new ErrorResponse();
        if (!isValid(startDateStr, endDateStr)) {
            errorResponse.setCode(ErrorCodeConstants.INVALID_DATE_RANGE_CODE);
            errorResponse.setMessage(ErrorCodeConstants.INVALID_DATE_RANGE_STRING);
        }
        return errorResponse;
    }
    
    private static boolean isValid(String startDateStr, String endDateStr) {
        boolean isValid = false;
        
        try {
            // Need to make sure startDate and endDate are valid before checking date range.
            Date startDate = TimeStringUtil.parseStringToDate(startDateStr);
            Date endDate = TimeStringUtil.parseStringToDate(endDateStr);
            isValid = !(endDate.before(startDate));
        } catch (Exception e) {
            isValid = false;
        }
        
        return isValid;
    }
}
