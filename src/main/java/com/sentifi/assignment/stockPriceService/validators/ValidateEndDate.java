package com.sentifi.assignment.stockPriceService.validators;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to validate End Date.
* 
*  RULES:           1) Should be valid format YYYY-MM-DD.
*/
/***************************************************************************/

import java.text.ParseException;

import com.sentifi.assignment.stockPriceService.constants.ErrorCodeConstants;
import com.sentifi.assignment.stockPriceService.util.TimeStringUtil;

public abstract class ValidateEndDate {

    public static ErrorResponse checkValid(String endDateStr) {
        ErrorResponse errorResponse = new ErrorResponse();
        if (!isValid(endDateStr)) {
            errorResponse.setCode(ErrorCodeConstants.INVALID_ENDDATE_FORMAT_CODE);
            errorResponse.setMessage(ErrorCodeConstants.INVALID_ENDDATE_FORMAT_STRING);
        }
        return errorResponse;
    }
    
    private static boolean isValid(String endDateStr) {
        boolean isValid = false;
        if (endDateStr != null && !endDateStr.isEmpty()) {
            try {
                TimeStringUtil.parseStringToDate(endDateStr);
                isValid = true;
            }
            catch (ParseException e) {
                isValid = false;
            }
        }
        return isValid;
    }
}
