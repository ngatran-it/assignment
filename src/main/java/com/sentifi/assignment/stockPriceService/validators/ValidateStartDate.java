package com.sentifi.assignment.stockPriceService.validators;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to validate Start Date.
* 
*  RULES:           1) Should be valid format YYYY-MM-DD.
*/
/***************************************************************************/

import java.text.ParseException;

import com.sentifi.assignment.stockPriceService.constants.ErrorCodeConstants;
import com.sentifi.assignment.stockPriceService.util.TimeStringUtil;

public abstract class ValidateStartDate {

    public static ErrorResponse checkValid(String startDateStr) {
        ErrorResponse errorResponse = new ErrorResponse();
        if (!isValid(startDateStr)) {
            errorResponse.setCode(ErrorCodeConstants.INVALID_STARTDATE_FORMAT_CODE);
            errorResponse.setMessage(ErrorCodeConstants.INVALID_STARTDATE_FORMAT_STRING);
        }
        return errorResponse;
    }
    
    private static boolean isValid(String startDateStr) {
        boolean isValid = false;
        if (startDateStr != null && !startDateStr.isEmpty()) {
            try {
                TimeStringUtil.parseStringToDate(startDateStr);
                isValid = true;
            }
            catch (ParseException e) {
                isValid = false;
            }
        }
        return isValid;
    }
}
