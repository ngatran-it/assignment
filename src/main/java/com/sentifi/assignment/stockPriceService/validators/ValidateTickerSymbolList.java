package com.sentifi.assignment.stockPriceService.validators;
/*************************************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to validate Ticker Symbol List.
* 
*  RULES:           1) Should be a list of ticker symbol elements separated by a comma character.
*                   2) Number of elements should not be more than 1000.
*/
/*************************************************************************************************/

import com.sentifi.assignment.stockPriceService.constants.ApplicationConstants;
import com.sentifi.assignment.stockPriceService.constants.ErrorCodeConstants;

public abstract class ValidateTickerSymbolList {

    public static ErrorResponse checkValid(String tickerSymbolList) {
        ErrorResponse errorResponse = new ErrorResponse();
        String[] tickerSymbols = tickerSymbolList.split(ApplicationConstants.COMMA);
        
        if (tickerSymbols.length > 1000) {
            errorResponse.setCode(ErrorCodeConstants.INVALID_TICKER_SYMBOL_LIST_CODE);
            errorResponse.setMessage(String.format(ErrorCodeConstants.INVALID_TICKER_SYMBOL_LIST_STRING,
                    (tickerSymbols.length - 1000)));
        }
        return errorResponse;
    }
}
