package com.sentifi.assignment.stockPriceService.values;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to display price details.
/***************************************************************************/

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class PriceDisplayVO {
    
    private String             ticker;
    private List<List<String>> dateClose;
    private String             avg;
    private List<Object>       errorList;
    
    /**
     * @return the ticker
     */
    public String getTicker() {
        return ticker;
    }
    
    /**
     * @param ticker the ticker to set
     */
    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    /**
     * @return the dateClose
     */
    public List<List<String>> getDateClose() {
        return dateClose;
    }

    /**
     * @param dateClose the dateClose to set
     */
    public void setDateClose(List<List<String>> dateClose) {
        this.dateClose = dateClose;
    }

    /**
     * @return the avg
     */
    public String getAvg() {
        return avg;
    }

    /**
     * @param avg the avg to set
     */
    public void setAvg(String avg) {
        this.avg = avg;
    }

    /**
     * @return the errorList
     */
    public List<Object> getErrorList() {
        return errorList;
    }

    /**
     * @param errorList the errorList to set
     */
    public void setErrorList(List<Object> errorList) {
        this.errorList = errorList;
    }
}
