package com.sentifi.assignment.stockPriceService.values;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to display Quandl dataset details.
/***************************************************************************/

import java.util.List;

public class QuandlDatasetDisplayVO {
    private long               id;
    private String             start_date;
    private String             end_date;
    private String             newest_available_date;
    private String             oldest_available_date;
    private List<List<String>> data;
    
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the start_date
     */
    public String getStart_date() {
        return start_date;
    }

    /**
     * @param start_date the start_date to set
     */
    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    /**
     * @return the end_date
     */
    public String getEnd_date() {
        return end_date;
    }

    /**
     * @param end_date the end_date to set
     */
    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    /**
     * @return the data
     */
    public List<List<String>> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(List<List<String>> data) {
        this.data = data;
    }

    /**
     * @return the newest_available_date
     */
    public String getNewest_available_date() {
        return newest_available_date;
    }

    /**
     * @param newest_available_date the newest_available_date to set
     */
    public void setNewest_available_date(String newest_available_date) {
        this.newest_available_date = newest_available_date;
    }

    /**
     * @return the oldest_available_date
     */
    public String getOldest_available_date() {
        return oldest_available_date;
    }

    /**
     * @param oldest_available_date the oldest_available_date to set
     */
    public void setOldest_available_date(String oldest_available_date) {
        this.oldest_available_date = oldest_available_date;
    }
}
