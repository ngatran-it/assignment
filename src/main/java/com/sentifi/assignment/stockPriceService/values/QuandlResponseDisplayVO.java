package com.sentifi.assignment.stockPriceService.values;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to display Quandl response details.
/***************************************************************************/

public class QuandlResponseDisplayVO {
    
    private QuandlDatasetDisplayVO dataset;
    private Object quandl_error;

    /**
     * @return the dataset
     */
    public QuandlDatasetDisplayVO getDataset() {
        return dataset;
    }

    /**
     * @param dataset the dataset to set
     */
    public void setDataset(QuandlDatasetDisplayVO dataset) {
        this.dataset = dataset;
    }

    /**
     * @return the quandl_error
     */
    public Object getQuandl_error() {
        return quandl_error;
    }

    /**
     * @param quandl_error the quandl_error to set
     */
    public void setQuandl_error(Object quandl_error) {
        this.quandl_error = quandl_error;
    }
}
