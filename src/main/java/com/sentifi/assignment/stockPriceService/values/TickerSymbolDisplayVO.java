package com.sentifi.assignment.stockPriceService.values;
/***************************************************************************
//* Copyright(c)    Sentifi, 2019
//* Author:         Nga Tran
//* Created On:     Jan 13, 2019
//* 
//* Description:    Class is to display Ticker Symbol details.
/***************************************************************************/

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TickerSymbolDisplayVO {
    
    private List<PriceDisplayVO> prices;
    private PriceDisplayVO       dma200;
    private String               newestAvailableDate;

    /**
     * @return the prices
     */
    public List<PriceDisplayVO> getPrices() {
        return prices;
    }

    /**
     * @param prices the prices to set
     */
    public void setPrices(List<PriceDisplayVO> prices) {
        this.prices = prices;
    }

    /**
     * @return the dmaFor200
     */
    public PriceDisplayVO getDma200() {
        return dma200;
    }

    /**
     * @param dmaFor200 the dmaFor200 to set
     */
    public void setDma200(PriceDisplayVO dma200) {
        this.dma200 = dma200;
    }

    /**
     * @return the newestAvailableDate
     */
    public String getNewestAvailableDate() {
        return newestAvailableDate;
    }

    /**
     * @param newestAvailableDate the newestAvailableDate to set
     */
    public void setNewestAvailableDate(String newestAvailableDate) {
        this.newestAvailableDate = newestAvailableDate;
    }
}
